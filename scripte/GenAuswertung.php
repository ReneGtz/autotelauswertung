<?php

echo "Konvertiere Datum\n";
if(empty($argv[1])){
	$time = time();
} else if(is_numeric($argv[1]) && strpos($argv[1], '.') === false) {
	$time = time();
	switch($argv[1]){
		case 0: //automatic weekday detection
			switch(date('w', $time)){
				case 0: //sunday
					$time -= 172800; //172.800 = 2*(60*60*24)
					break 2;
					
				case 1: //monday
					$time -= 259200; //259.200 = 3*(60*60*24)
					break 2;
					
				//default: //from tuesday to saturday
					//go to next case
			}
		case 1: //yesterday
			$time -= 86400; //86400 = 60*60*24
			break;
		default:
			echo 'Unbekannte Variablen angabe.';
			die;
	}
	 	
} else { //if strings is given as date delimiter
	$time = strtotime($argv[1]);
	if(!is_numeric($time)){
		echo 'Invalide Angabe des Datums!';
		die;
	}
}

$firstDate = date('Y-m-d', $time);
$secondDate = date('Y-m-d', ($time + 86400));
if(empty($firstDate) || empty($secondDate) || empty($time)){
	echo 'Invalider Aufruf!';
	die;
}
echo "Datum festgelegt auf '".$firstDate."'.\nVerbindsung zur Datenbank.\n";

$DB = new PDO("sqlsrv:Server=---;Database=---;ConnectionPooling=0",'---', '---');

echo "Bezihen der Daten\n";
/** total of lost calls **/
$stm_LostCalls = $DB->prepare("---");
$stm_LostCalls->bindParam(':DateOne', $firstDate);
$stm_LostCalls->bindParam(':DateTwo', $secondDate);
$stm_LostCalls->execute();
$arr_LostCalls = $stm_LostCalls->fetchAll(PDO::FETCH_NUM);
$stm_LostCalls->closeCursor();


/** separate office **/
$stm_KzS = $DB->prepare("---");
$stm_KzS->bindParam(':DateOne', $firstDate);
$stm_KzS->bindParam(':DateTwo', $secondDate);
$stm_KzS->execute();
$arr_KzS = $stm_KzS->fetchAll(PDO::FETCH_NUM);
$stm_KzS->closeCursor();

/** To 8 o'clock **/
$stm = $DB->prepare("---");

$stm->bindValue(':DateOne', $firstDate.' 06:00:01');
$stm->bindValue(':DateTwo', $firstDate.' 08:00:00');
$stm->execute();
$arr_To8 = $stm->fetchAll(PDO::FETCH_NUM);
$stm->closeCursor();

/** To 9 o'clock **/
$stm = $DB->prepare("---");
$stm->bindValue(':DateOne', $firstDate.' 08:00:01');
$stm->bindValue(':DateTwo', $firstDate.' 09:00:00');
$stm->execute();
$arr_To9 = $stm->fetchAll(PDO::FETCH_NUM);
$stm->closeCursor();

/** $DB 17 o'clock **/
$stm = $DB->prepare("---");
$stm->bindValue(':DateOne', $firstDate.' 09:00:01');
$stm->bindValue(':DateTwo', $firstDate.' 17:00:00');
$stm->execute();
$arr_To17 = $stm->fetchAll(PDO::FETCH_NUM);
$stm->closeCursor();

/** close database connection **/
$stm_LostCalls = NULL;
$stm_KzS = NULL;
$stm = NULL;
$DB = NULL;

echo "Erstellen der Excel-datei\n";
require_once 'PHPExcel/PHPExcel.php';

$objPHPExcel = new PHPExcel();
$activeWks = $objPHPExcel->setActiveSheetIndex(0);
$activeWks->setTitle('Auswertung');

/** A1 **/
$activeWks->setCellValue('A1', 'Anrufstatistik '.date('d.m.Y', $time));
$activeWks->getStyle('A1')->applyFromArray($styleArray = array(
  'font' => array(
  	'size' => 16,
    'underline' => 'single',
    'italic' => true
  ),
  'alignment' => array(
  	#'horizontal' => 'center',
  	#'vertical' => 'center'
  )
));

/** I1 **/
$activeWks->setCellValue('I1', 'Verlorene Anrufe');
$activeWks->getStyle('I1')->applyFromArray($styleArray = array(
	'font' => array(
  		'size' => 14,
    	'underline' => 'single',
    	'italic' => true
  )
));

/** K1 **/
$ges = count($arr_LostCalls);
$Nach8 = 0;
#var_dump($Nach8);
foreach($arr_LostCalls AS $lost){
	$str_time = explode(' ',$lost[7])[1];
	if(intval($str_time[0]) == 0 && intval($str_time[1]) < 8 ){
		continue;
	}
	$Nach8++;
}
$activeWks->setCellValue('K1', $ges.' ( '.$Nach8.' nach 8 Uhr)');
$activeWks->getStyle('K1')->applyFromArray($styleArray = array(
	'font' => array(
  		'size' => 14,
  ),
  'alignment' => array(
  	'horizontal' => 'right',
  )
));



#unset($styleArray);
//$activeWks->getStyle("A1")->getFont()->setSize(16);
$activeWks->getRowDimension('1')->setRowHeight(30);
$activeWks->getColumnDimension('A')->setWidth(36);
$activeWks->getColumnDimension('B')->setWidth(0.6);
$activeWks->getColumnDimension('C')->setWidth(26);
$activeWks->getColumnDimension('D')->setWidth(0.6);
$activeWks->getColumnDimension('E')->setWidth(26);
$activeWks->getColumnDimension('F')->setWidth(0.6);
$activeWks->getColumnDimension('G')->setWidth(26);
$activeWks->getColumnDimension('H')->setWidth(0.6);
$activeWks->getColumnDimension('I')->setWidth(26);
$activeWks->getColumnDimension('J')->setWidth(0.6);
$activeWks->getColumnDimension('K')->setWidth(26);
$activeWks->getRowDimension('5')->setRowHeight(4);
//$sheet->mergeCells('A1:B1');

$HeadlineCenter = array(
  'font' => array(
    'bold' => true
  ),
  'borders' => array(
    'outline' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  ),
  'alignment' => array(
  	'horizontal' => 'center'
));

$HeadlineLeft = array(
  'font' => array(
    'bold' => true
  ),
  'borders' => array(
    'outline' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  ),
  'alignment' => array(
  	'horizontal' => 'left'
));
$HeadlineRight = array(
  'font' => array(
    'bold' => true
  ),
  'borders' => array(
    'outline' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  ),
  'alignment' => array(
  	'horizontal' => 'right'
));

$activeWks->setCellValue('A4', 'Benutzer / Name');
$activeWks->getStyle('A4')->applyFromArray($HeadlineLeft);

$activeWks->setCellValue('C4', 'Anrufe bis 8 Uhr');
$activeWks->getStyle('C4')->applyFromArray($HeadlineCenter);

$activeWks->setCellValue('E4', 'Anrufe von 8 Uhr bis 9 Uhr');
$activeWks->getStyle('E4')->applyFromArray($HeadlineCenter);

$activeWks->setCellValue('G4', 'Anrufe von 9 Uhr bis 17 Uhr');
$activeWks->getStyle('G4')->applyFromArray($HeadlineCenter);

$activeWks->setCellValue('I4', 'Küche zum Stern');
$activeWks->getStyle('I4')->applyFromArray($HeadlineCenter);

$activeWks->setCellValue('K4', 'Gesampt');
$activeWks->getStyle('K4')->applyFromArray($HeadlineCenter);


$ZeilenVorlage = array(
	0 => 0, 	//bis 8
	1 => 0,		//bis 9
	2 => 0,		//bis 17
	3 => 0,		//KzS
);

$inhalt = array();

foreach($arr_To8 AS $To8){
	if(!array_key_exists($To8[1], $inhalt)){
		$inhalt[$To8[1]] = $ZeilenVorlage;
	}
	$inhalt[$To8[1]][0] = $To8[0];
}

foreach($arr_To9 AS $To9){
	if(!array_key_exists($To9[1], $inhalt)){
		$inhalt[$To9[1]] = $ZeilenVorlage;
	}
	$inhalt[$To9[1]][1] = $To9[0];
}

foreach($arr_To17 AS $To17){
	if(!array_key_exists($To17[1], $inhalt)){
		$inhalt[$To17[1]] = $ZeilenVorlage;
	}
	$inhalt[$To17[1]][2] = $To17[0];
}

foreach($arr_KzS AS $ToKzS){
	if(!array_key_exists($ToKzS[1], $inhalt)){
		$inhalt[$ToKzS[1]] = $ZeilenVorlage;
	}
	$inhalt[$ToKzS[1]][3] = $ToKzS[0];
}

$styleArray3 = array(
  'borders' => array(
    'outline' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  ),
  'alignment' => array(
  	'horizontal' => 'left'
  )
);
$styleArray4 = array(
  'borders' => array(
    'outline' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  ),
  'alignment' => array(
  	'horizontal' => 'right'
  )
);

$i = 6;
foreach($inhalt AS $key => $zeile){
	if($key === 0){
		#echo $key;
		continue;
	}
	if(empty($key)){
		continue;
	} else if($key === '---, ---'){
		$key = '---';
	} else if($key === '---'){
		$key = '---';
	}
	
	$activeWks->setCellValue('A'.$i, $key);
	$activeWks->getStyle('A'.$i)->applyFromArray($styleArray3);
	
	if(intval($zeile[0]) > 0){
		$activeWks->setCellValue('C'.$i, $zeile[0]);
	}
	$activeWks->getStyle('C'.$i)->applyFromArray($styleArray4);
	
	if(intval($zeile[1]) > 0){
	$activeWks->setCellValue('E'.$i, $zeile[1]);
	}
	$activeWks->getStyle('E'.$i)->applyFromArray($styleArray4);
	
	if(intval($zeile[2]) > 0){
	$activeWks->setCellValue('G'.$i, $zeile[2]);
	}
	$activeWks->getStyle('G'.$i)->applyFromArray($styleArray4);
	
	if(intval($zeile[3]) > 0){
	$activeWks->setCellValue('I'.$i, $zeile[3]);
	}
	$activeWks->getStyle('I'.$i)->applyFromArray($styleArray4);
	

	$activeWks->setCellValue('K'.$i, '=C'.$i.'+E'.$i.'+G'.$i.'+I'.$i);
	$activeWks->getStyle('K'.$i)->applyFromArray($styleArray4);
	$i++;
}
$lastBodyRow = $i-1;


/** Fußzeile **/
$activeWks->getRowDimension($i)->setRowHeight(4);
$i++;
$activeWks->setCellValue('A'.$i, 'GESAMPT ANRUFE');
$activeWks->getStyle('A'.$i)->applyFromArray($HeadlineLeft);
$activeWks->setCellValue('C'.$i, '=SUM(C6:C'.$lastBodyRow.')');
$activeWks->getStyle('C'.$i)->applyFromArray($HeadlineRight);
$activeWks->setCellValue('E'.$i, '=SUM(E6:E'.$lastBodyRow.')');
$activeWks->getStyle('E'.$i)->applyFromArray($HeadlineRight);
$activeWks->setCellValue('G'.$i, '=SUM(G6:G'.$lastBodyRow.')');
$activeWks->getStyle('G'.$i)->applyFromArray($HeadlineRight);
$activeWks->setCellValue('I'.$i, '=SUM(I6:I'.$lastBodyRow.')');
$activeWks->getStyle('I'.$i)->applyFromArray($HeadlineRight);
$activeWks->setCellValue('K'.$i, '=SUM(K6:K'.$lastBodyRow.')');
$activeWks->getStyle('K'.$i)->applyFromArray($HeadlineRight);

$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$objWriter->setPreCalculateFormulas(true);
$fileName = $_SERVER['USERPROFILE'].'\Desktop'.'\AW-'.date('d-m-Y', $time).'.xlsx';
$objWriter->save($fileName);

if(file_exists($fileName)){
	echo 'Datei Erfolgreich auf dem Desktop erstellt.'."\n";
} else {
	echo 'Konnte Datei nicht auf dem Desktop erstellen.'."\n";
	die;
}

if(copy($fileName, '---\AW-'.date('d-m-Y', $time).'.xlsx')){
	echo 'Datei erfolgreich nach A: Kopiert.'."\n";
} else {
	echo 'Konnte die Datei nicht ins Laufwerk A Kopieren!'."\n";
}

?>